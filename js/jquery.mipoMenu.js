(function(){
    $.fn.mipoMenu = function( options ) {
        var menu = $(this);
        options = options || {};

        var changeColors = options.changeColors || false;
        var animationDuration = options.animationDuration || 500;
        var activeMenuItemColor = options.activeMenuItemColor || "#3c4d54";
        var activeMenuItemClass = options.activeMenuItemClass || "active";

        var menuHeight = menu.height();
        var menuWidth = menu.width();
        var menuTop = menu.position().top;
        var oldPosition = menu.css("position");
        menu.css({
            "z-index": 100,
            "position": "fixed"
        });

        menu.find("a").each(function () {
            var menuItem = $(this).parent();
            var block = $( $(this).attr("href"));
            if (block.length) {
                menuItem.data("top", block.position().top);
                menuItem.data("height", block.height());
            }
        }).click(function () {
            var menuItem = $(this).parent();
            $('html, body').animate({
                scrollTop: menuItem.data("top") - menuHeight
            }, animationDuration);
            return false;
        });
        menu.css("position", oldPosition);

        $(window).scroll(function () {
            var top = $(window).scrollTop();
            var height = $(window).height();

            if (top > menuTop) {
                menu.css({
                    "position": "fixed",
                    "top": "0px",
                    "width": menuWidth + "px"
                });
            } else {
                menu.css("position", "inherit");
            }

            var bestScore = 0, best = null;
            menu.find("li").each(function () {
                var menuItem = $(this);
                menuItem.removeClass(activeMenuItemClass);
                var mTop = menuItem.data("top");
                var mHeight = menuItem.data("height");
                var visiblePixels = 0;

                if (mTop <= top && mTop + mHeight > top) {
                    visiblePixels = mTop + mHeight - top;
                    if (visiblePixels > height) {
                        visiblePixels = height;
                    }
                } else if (mTop < top + height && mTop + mHeight > top) {
                    visiblePixels = top + height - mTop;
                    if (visiblePixels > height) {
                        visiblePixels = height;
                    }
                }

                if (visiblePixels > 0) {
                    var rgbaCol = 'rgba(' + parseInt(activeMenuItemColor.slice(-6,-4),16)
                        + ',' + parseInt(activeMenuItemColor.slice(-4,-2),16)
                        + ',' + parseInt(activeMenuItemColor.slice(-2),16)
                        +',' + (visiblePixels / height) + ')';

                    if (changeColors)
                        menuItem.css('background-color', rgbaCol);

                    if (bestScore < visiblePixels) {
                        bestScore = visiblePixels;
                        best = menuItem;
                    }
                } else if (changeColors) {
                    menuItem.css('background-color', "transparent")
                }
            });

            if (best) {
                best.addClass(activeMenuItemClass);
            }
        });
        $(window).scroll();
    }
})(jQuery);